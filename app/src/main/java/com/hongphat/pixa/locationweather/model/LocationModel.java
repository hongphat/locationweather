package com.hongphat.pixa.locationweather.model;


import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 9/15/2016.
 */
public class LocationModel {

    @com.google.gson.annotations.SerializedName("coord")
    public Coord coord;
    @com.google.gson.annotations.SerializedName("weather")
    public List<Weather> weather;
    @com.google.gson.annotations.SerializedName("base")
    public String base;
    @com.google.gson.annotations.SerializedName("main")
    public Main main;
    @com.google.gson.annotations.SerializedName("visibility")
    public int visibility;
    @com.google.gson.annotations.SerializedName("wind")
    public Wind wind;
    @com.google.gson.annotations.SerializedName("clouds")
    public Clouds clouds;
    @com.google.gson.annotations.SerializedName("dt")
    public int dt;
    @com.google.gson.annotations.SerializedName("sys")
    public Sys sys;
    @SerializedName("id")
    public String id;
    @com.google.gson.annotations.SerializedName("name")
    public String name;
    @com.google.gson.annotations.SerializedName("cod")
    public int cod;

    public static class Coord {
        @com.google.gson.annotations.SerializedName("lon")
        public double lon;
        @com.google.gson.annotations.SerializedName("lat")
        public double lat;
    }

    public static class Weather {
        @com.google.gson.annotations.SerializedName("id")
        public int id;
        @com.google.gson.annotations.SerializedName("main")
        public String main;
        @com.google.gson.annotations.SerializedName("description")
        public String description;
        @com.google.gson.annotations.SerializedName("icon")
        public String icon;
    }

    public static class Main {
        @com.google.gson.annotations.SerializedName("temp")
        public double temp;
        @com.google.gson.annotations.SerializedName("pressure")
        public int pressure;
        @com.google.gson.annotations.SerializedName("humidity")
        public int humidity;
        @com.google.gson.annotations.SerializedName("temp_min")
        public double temp_min;
        @com.google.gson.annotations.SerializedName("temp_max")
        public double temp_max;
    }

    public static class Wind {
        @com.google.gson.annotations.SerializedName("speed")
        public double speed;
        @com.google.gson.annotations.SerializedName("deg")
        public int deg;
    }

    public static class Clouds {
        @com.google.gson.annotations.SerializedName("all")
        public int all;
    }

    public static class Sys {
        @com.google.gson.annotations.SerializedName("type")
        public int type;
        @com.google.gson.annotations.SerializedName("id")
        public int id;
        @com.google.gson.annotations.SerializedName("message")
        public double message;
        @com.google.gson.annotations.SerializedName("country")
        public String country;
        @com.google.gson.annotations.SerializedName("sunrise")
        public int sunrise;
        @com.google.gson.annotations.SerializedName("sunset")
        public int sunset;
    }


}
