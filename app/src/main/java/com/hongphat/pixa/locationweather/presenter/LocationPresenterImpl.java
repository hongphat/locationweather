package com.hongphat.pixa.locationweather.presenter;

import android.util.Log;

import com.hongphat.pixa.locationweather.model.LocationModel;
import com.hongphat.pixa.locationweather.rest.ApiClient;
import com.hongphat.pixa.locationweather.rest.ApiInterface;
import com.hongphat.pixa.locationweather.view.LocationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Admin on 9/16/2016.
 */
public class LocationPresenterImpl implements LocationPresenter {
    private LocationView locationView;
    private final static String API_KEY = "712d0edf8732db2a90813e3d34be21fb";
    public LocationPresenterImpl(LocationView locationView){
        this.locationView = locationView;
    }

    @Override
    public void getData(String location) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<LocationModel> call = apiService.getLocation(location, API_KEY);
        call.enqueue(new Callback<LocationModel>() {
            @Override
            public void onResponse(Call<LocationModel> call, Response<LocationModel> response) {
                Log.i("App info", "Response");
                if (response.body() == null) {
                    Log.i("App info", "Body null");
                }
                locationView.getSuccess(response.body().name,response.body().id);
            }

            @Override
            public void onFailure(Call<LocationModel> call, Throwable t) {
                Log.i("App info", "Fail");
            }

        });
    }
}
