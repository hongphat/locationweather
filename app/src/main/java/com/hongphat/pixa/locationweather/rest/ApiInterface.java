package com.hongphat.pixa.locationweather.rest;

import com.hongphat.pixa.locationweather.model.LocationModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Admin on 9/14/2016.
 */
public interface ApiInterface {

    @GET("/data/2.5/weather")
    Call<LocationModel> getLocation(@Query("q") String place, @Query("appid") String apiKey);

}
