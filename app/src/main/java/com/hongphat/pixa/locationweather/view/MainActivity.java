package com.hongphat.pixa.locationweather.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.hongphat.pixa.locationweather.R;
import com.hongphat.pixa.locationweather.presenter.LocationPresenter;
import com.hongphat.pixa.locationweather.presenter.LocationPresenterImpl;

public class MainActivity extends AppCompatActivity implements LocationView {

    TextView idText, cityText;
    private LocationPresenter presenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        idText = (TextView)findViewById(R.id.id);
        cityText = (TextView)findViewById(R.id.city);

        presenter = new LocationPresenterImpl(this);
        presenter.getData("london");
    }

    @Override
    public void getSuccess(String city, String id) {
        idText.setText(city);
        cityText.setText(id);
    }
}
