package com.hongphat.pixa.locationweather.presenter;

/**
 * Created by Admin on 9/16/2016.
 */
public interface LocationPresenter {
    void getData(String location);
}
